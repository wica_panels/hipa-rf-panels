# Overview 

This is the **hipa-rf-panels** git repository which provides a set of wica panels for visualising the
state of HIPA's RF Systems. 
 
The project is an NPM project which uses the rollup.js package to create a single RF support file ('rf-support.js')
for inclusion in the html source files.

# How to Develop

## How to edit

Edit the source files in the /src directory. 

## How to build

The npm 'build' script creates the build directory from the files in the src directory. You run it as follows:
```
  npm run build
```

## How to deploy

Copy the contents of the build directory to the desired deployment directories 'dist_dev'/'dist_prod'/'dist_ext'. 
The npm 'dist_publish_all' script commits the latest changes to the local repository and pushes the changes to the
corresponding branches on the GitLab Server. PSI's autodeployer then takes care of publication on the relevant
wica development, prooduction or external servers.
```
  npm run dist_publish_all
```

# Plotly ES6 Module Library Bug

With Plotly version 2.2.0 the bug that previously prevented the package loading as an ES6 module has been fixed.
So the ugly library patch described in previous versions of this README file is no longer required.


# Project Changes and Tagged Releases

* See the [CHANGELOG.md](CHANGELOG.md) file for further information.

