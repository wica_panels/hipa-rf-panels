console.debug( "Executing script in rf-status-support.js...");

/**
* Updates the visual state of the specified HTML element to reflect the latest
* numeric value obtained from the WicaChannel associated with the supplied
* event object.
*
* @param event specifies the widget whose wica value has undergone a change
*    of state.
* @param okThreshold numeric threshold above which the ok color should be displayed.
* @param {String} okColor the color to be used to render the widget when the
*    value of the channel is above the specified threshold.
*/
// eslint-disable-next-line no-unused-vars
function updateStatusWidgetFromChannelWithNumericValue( event, okThreshold, okColor ) {
    const widget = event.target;
    const channelName = event.channelName;
    const tooltip = channelName + ": value > " + okThreshold;
    const value =  event.channelValueLatest;
    const isOkFunc =  function( value )
    {
        return value  > okThreshold;
    }
    updateStatusWidget( widget, value, isOkFunc, okColor, tooltip );
}
/**
 * Updates the visual state of the specified HTML element to reflect the latest
 * boolean value obtained from the WicaChannel associated with the supplied
 * event object.
 *
 * @param event specifies the widget whose wica value has undergone a change
 *    of state.
 *
 * @param {String} okColor the color to be used to render the widget when the
 *    value of the channel is set to 'True'.
 */
// eslint-disable-next-line no-unused-vars
function updateStatusWidgetFromChannelWithBooleanState( event, okColor ) {
    const widget = event.target;
    const channelName = event.channelName;
    const tooltip = channelName + ": value = 'True'";
    const value =  event.channelValueLatest;
    const isOkFunc =  function( value )
    {
        return value  === 'True';
    }
    updateStatusWidget( widget, value, isOkFunc, okColor, tooltip );
}

/**
 * Updates the visual state of the specified HTML element to reflect the specified bit
 * within the latest value obtained from the WicaChannel associated with the supplied
 * event object.
 *
 * @param event specifies the widget whose wica value has undergone a change
 *    of state.
 *
 * @param bitSelect the index of the bit to select 0, 1, 2, 3 from within
 *    the wica channel value.
 *
 * @param {String} okColor the color to be used to render the widget when the
 *    selected bit is set.
 */
// eslint-disable-next-line no-unused-vars
function updateStatusWidgetFromChannelWithMultipleStatusBits( event, bitSelect, okColor ) {
    const widget = event.target;
    const channelName = event.channelName;
    const tooltip = channelName + ": value & BIT_" + bitSelect + " = BIT_" + bitSelect;
    const value =  event.channelValueLatest;
    const bitOfInterest = 1 << bitSelect;
    const isOkFunc =  function( value )
    {
        return ( value & bitOfInterest ) === bitOfInterest
    }
    updateStatusWidget( widget, value, isOkFunc, okColor, tooltip );
}

/**
 * Updates the visual state of the specified HTML element to reflect the latest value obtained
 * from the Wica Channel.
 *
 * @param {HTMLElement } widget the widget to update.
 * @param {import().WicaChannelValue} wicaChannelValue set to null if the channel is offline.
 * @param {Function} isOkFunc the function to be applied to the current value to
 * determine whether the current value is OK or NOT OK.
 * @param okColor the color to be used to render the widget when the isOkFunc returns true.
 * @param {String} tooltip the text to be displayed when hovering the cursor over the element.
 */
function updateStatusWidget( widget, wicaChannelValue, isOkFunc, okColor, tooltip )
{
    if ( wicaChannelValue.val === null )
    {
        widget.style.backgroundColor = "white";
    }
    else
    {
        if ( isOkFunc( wicaChannelValue.val ) )
        {
            widget.style.backgroundColor = okColor;
        }
        else
        {
            widget.style.backgroundColor = "lightgray";
        }
    }
    widget.setAttribute( "data-wica-tooltip", tooltip )
}
