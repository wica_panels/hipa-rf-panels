console.debug( "Executing script in rf-svg-support.js...");

let svgDoc = null;

function init_svg_document_ref()
{
    if ( svgDoc === null ) {
        const svgElement = document.getElementById( "svgHostElement" );
        svgDoc = svgElement.contentDocument;
    }
}

// eslint-disable-next-line no-unused-vars
function update_svg_numeric_value( event, svgEleId, fractionDigits, optUnits = false, optExp = false )
{
    init_svg_document_ref();
    const targetElement = svgDoc.getElementById( svgEleId );
    if ( targetElement == null ) {
        return;
    }

    const target = event.target;
    const streamConnected = target.hasAttribute( "data-wica-stream-state" ) && target.getAttribute( "data-wica-stream-state" ).includes( "opened-" );
    if ( ! streamConnected  )
    {
        targetElement.textContent = "Wica Stream NC";
        targetElement.style.fill = "lightgray";
        return;
    }

    const rawValue = event.channelValueLatest.val;
    if ( rawValue === null )
    {
        targetElement.textContent = "EPICS Channel NC";
        targetElement.style.fill = "lightgray";
        return;
    }

    const formattedValue = optExp ? rawValue.toExponential( fractionDigits ):  rawValue.toFixed( fractionDigits );
    const units = optUnits ? event.channelMetadata.egu : "";
    targetElement.textContent = formattedValue + " " + units;
    targetElement.style.fill="black";

}