# Overview

This log describes the functionality of tagged versions within the repository.

# Tags  
## [Version 1.0.0](https://gitlab.psi.ch/wica_panels/hipa-rf-panels/tags/1.0.0)
  First tagged release following upgrade triggered by Matthias Stoll.
  See CTRLIT-7641.

## [Version 1.1.0](https://gitlab.psi.ch/wica_panels/hipa-rf-panels/tags/1.1.0)
  First step towards implementing latest requests of Matthias Stoll for LLRF Group.
  See LLRF-1519 for further details.
  No support yet for display of main SVG graphic.
  
  ##### Change List
  * Fix bug which resulted in a console error message during page load.
  * Fix bug which caused pressure display to be shown in incorrect fromat.  
  * Implement all requests from LLRF-1519 (except no support yet for overview graphic).
  * Eliminated unnecessary code.
  * Eliminated redundant wica directory. Wica is loaded from the server.

## [Version 2.0.0](https://gitlab.psi.ch/wica_panels/hipa-rf-panels/tags/2.0.0)
  Completed requests of Matthias Stoll. Implementation is designed to be
  visually an almost exact copy of the LLRF CaQtDm panels. Now includes
  SVG. Ready for review. 
 
  #### Possible Future Improvements
  * Create single rf-support file.
  * Might be cleaner to keep beam current monitoring separate from main graphic.
  * Consider updating legend: PFWD -> Forward Power; PREV -> Reverse Power; Voltage -> High Voltage ?
 
  ##### Change List
  * Disabled interaction possibilities since interaction with the plots makes little sense in this context.
  * Upgraded dependencies for latest.
  * Split off plot support into separate file.
  * Split off support for SVG into separate file.
  * Split off support for status updates into separate file.
  * Symbols for use in the RF SVG.
  * New RF SVG.
  * Updated for compatibility with latest HIPA caQtDM Panels.
  * Turned of beautify which no longer seems to be supported. Updated to latest rollup plugins. Now deploys svg and status JS support files separately.
  * Updated for compatibility with new vacuum channel. Adjusted plot scaling as per HIPA caQtDM panels.

## [Version 2.0.1](https://gitlab.psi.ch/wica_panels/hipa-rf-panels/tags/2.0.1)
  Fixed bug with missing tooltips.
 
  #### Possible Future Improvements
  * Create single rf-support file.
  * Might be cleaner to keep beam current monitoring separate from main graphic.
  * Consider updating legend: PFWD -> Forward Power; PREV -> Reverse Power; Voltage -> High Voltage ?
 
  ##### Change List
  * Suppress IntelliJ warning messages.
  * Re-enabled display of channel names on hover but disabled dragmode which does not bring anything but ability to
    screw up the plots in this context.

## [Version 2.1.0](https://gitlab.psi.ch/wica_panels/hipa-rf-panels/tags/2.1.0)
  Further updates following latest feedback from Matthias Stoll. See [LLRF-1519](https://jira.psi.ch/browse/LLRF-1519)
  
  #### Possible Future Improvements
  * Create single rf-support file.
  * Consider updating legend: PFWD -> Forward Power; PREV -> Reverse Power; Voltage -> High Voltage ?
 
  ##### Change List
  * Cavity Status Plots: changed constants to upper case. 
  * Cavity Phase Plots: Added support for colourising phase-plot border to reflect phase status
  * Changed scaling on CWB cavity. 
  * Changed CWB units to Watts (W).
  * Updated SVG to follow latest caqtdm example.
  * Addressed issues following latest feedback from Matthias Stoll.
  * Updated with details of release 2.1.0
  * Re-enabled display of channel names on hover.
  * Suppress IntelliJ warning messages where they do not bring anything.
  
## [Version 2.1.1](https://gitlab.psi.ch/wica_panels/hipa-rf-panels/tags/2.1.1) Released 2020-09-24
  Bug fixes from previous (2.1.0) release.
  
  #### Possible Future Improvements
  * Create single rf-support file.
  * Consider updating legend: PFWD -> Forward Power; PREV -> Reverse Power; Voltage -> High Voltage ?
 
  ##### Change List
  * 2020-09-24: Change SVG channel name for MWC2 to "MWC2:IST:2" following feedback from Matthias.

## [Version 2.1.2](https://gitlab.psi.ch/wica_panels/hipa-rf-panels/tags/2.1.1) Released 2021-02-18
  Bug fixes from previous (2.1.1) release.

  #### Possible Future Improvements
  * Create single rf-support file.
  * Consider updating legend: PFWD -> Forward Power; PREV -> Reverse Power; Voltage -> High Voltage ?

  ##### Change List
  * 2021-02-18: Changed amplifier widgets to show HF not EIN following feedback from Matthias.
                See this tracker: https://jira.psi.ch/browse/CTRLIT-8637


## [Version 2.2.0](https://gitlab.psi.ch/wica_panels/hipa-rf-panels/tags/2.2.0)

## Main Points:
- Adapted channel names for the phase plots to reflect changes in HIPA. Updated plot library.

## Detailed commit information:
- CHORE: Adapt channel names for phase plots to reflect changes at HIPA.
- BUG FIX: Upgrade plotly to latest (2.2.0). This library can be loaded as an ES6 module without patching.
- CHORE: Upgrade other dependencies to latest versions.
- CHORE: Upgrade linter config file to use JSON.
- CHORE: Retire npm-scripts-info as it has a security issue.
- CHORE: Create release 2.2.0

### Release Date
- 2023-04-17


## [Version 2.3.0](https://gitlab.psi.ch/wica_panels/hipa-rf-panels/tags/2.3.0)

## Main Points:
- Reenable Cavity 2 Plot as requested by Roger Kalt.

## Detailed commit information:
- ENHANCEMENT: Fix #1: Reenable Resonator 2 of Injector 2.
- BUG FIX: Suppress default text rendering on re-enabled B2 Plot.
- CHORE: Cleanup variable naming around bII.
- CHORE: Update dependencies to latest.
- ENHANCEMENT: Create release 2.3.0

### Release Date
- 2024-04-15


## [Version 2.3.1](https://gitlab.psi.ch/wica_panels/hipa-rf-panels/tags/2.3.1)

## Main Points:
- Fix Injector 2 Phase Plot regression bug as reported by Matthias Stoll.

## Detailed commit information:
- BUG FIX: Fix Injector 2 Phase Plot regression bug as reported by Matthias Stoll.
- BUG FIX: Create release 2.3.1

### Release Date
- 2024-06-04


## [Version 2.4.0](https://gitlab.psi.ch/wica_panels/hipa-rf-panels/tags/2.4.0)

## Main Points:
- ENHANCEMENT: Add support for Injector 2 resonator 2 and 4 monitoring as requested by Roger Kalt.

## Detailed commit information:
- CHORE: Update node dependencies to latest.
- ENHANCEMENT: Add support for Injector 2 resonator 2 and 4 monitoring.
- CHORE: Eliminate unneeded dependency.
- BUG FIX: Create release 2.4.0

### Release Date
- 2024-06-27


## [Version 2.5.0](https://gitlab.psi.ch/wica_panels/hipa-rf-panels/tags/2.5.0)

## Main Points:
- ENHANCEMENT: Deploy latest changes from Roger Kalt.

## Detailed commit information:
- ENHANCEMENT: add beam current, cav. input power rev. max.
- ENHANCEMENT: Change P-MAX to P-AVG.
- ENHANCEMENT: Create release 2.5.0

### Release Date
- 2024-10-08


