import * as log from "./logger.js"
import Plotly from "plotly.js-dist-min";
import JSON5 from 'json5';

const GUARD_WIDTH_IN_PERCENT = 1;
const NEEDLE_WIDTH_IN_PERCENT = 4;

export class CavityPhaseMonitor
{
    constructor( plotTitle, plotDivId, phaseDataId, phaseStatusDataId, rangeMin, rangeMax )
    {
        this.plotTitle = plotTitle;
        this.plotElement = document.getElementById( plotDivId );
        this.phaseDataElement = document.getElementById( phaseDataId );
        this.phaseStatusDataElement = document.getElementById( phaseStatusDataId );
        this.rangeMin = rangeMin;
        this.rangeMax = rangeMax;

        this.phaseDataElement.onchange = function dummy1() {};
        this.phaseStatusDataElement.onchange = function dummy2() {};

        this.layout = {
            title: this.plotTitle,
            titlefont: { size: 14, color: 'darkblue' },
            autosize: true,
            showlegend: false,
            margin: {l: 10, r: 10, b: 10, t: 30},
            paper_bgcolor: 'rgba(0,0,0,0)',
            plot_bgcolor: 'rgba(0,0,0,0)',
            xaxis: { range : [ this.rangeMin, this.rangeMax ], dtick: 1, showticklabels: false, linecolor: 'limegreen', linewidth: 3, mirror: true },
            yaxis: { showticklabels: false, linecolor: 'limegreen', linewidth: 3, mirror: true  },
            barmode: 'stack',
            hovermode: 'x',
            dragmode: false,
            hoverlabel: { namelength: -1, font: { size: 10 } }
        };
    }

    /**
     * Activates the plot. That's to stay starts rendering the associated plot object
     * based on information obtained from the configured wica html elements.
     *
     * @param {Number} [refreshRateInMilliseconds = 500] - The rate at which the plot should update.
     */
    activate( refreshRateInMilliseconds = 500 )
    {
        this.startPlotCycle( refreshRateInMilliseconds );
    }

    startPlotCycle( refreshRateInMilliseconds )
    {
        try
        {
            this.updatePlot();
        }
        catch( err )
        {
            CavityPhaseMonitor.logExceptionData_("Programming Error: updatePlot threw an exception: ", err );
        }

        // Reschedule next update
        setTimeout( () => this.startPlotCycle( refreshRateInMilliseconds ), refreshRateInMilliseconds );
    }

    updatePlot()
    {
        let tooltipText = CavityPhaseMonitor.buildFormattedTooltipText( this.phaseDataElement );

        // If the WICA Channel is disconnected create a trace array that will show this condition.
        if  ( ( ! ( this.phaseDataElement.hasAttribute( "data-wica-stream-state" ) ) ) ||
            ( ! ( this.phaseDataElement.hasAttribute( "data-wica-channel-value-latest" ) ) ) ||
            ( ! ( this.phaseDataElement.getAttribute( "data-wica-stream-state" ).startsWith( "opened-" ) ) ) )
        {
            let plotTraceObjects = this.makePlotlyTraceWicaStreamDisconnected( tooltipText );
            this.react( plotTraceObjects );
            return;
        }

        // Obtain the EPICS Value Objects for both the data phase and the data phase status
        let phaseDataValueObj = JSON5.parse( this.phaseDataElement.getAttribute( "data-wica-channel-value-latest" ) );
        let phaseDataStatusObj = JSON5.parse( this.phaseStatusDataElement.getAttribute( "data-wica-channel-value-latest" ) );

        // If either of the EPICS Channels are disconnected create a trace array that will show this condition.
        if ( ( phaseDataValueObj.val === null ) || ( phaseDataStatusObj.val === null ) )
        {
            let plotTraceObjects = this.makePlotlyTraceEpicsChannelDisconnected( tooltipText );
            this.react( plotTraceObjects );
            return;
        }

        // If the EPICS Channel Phase Status is non-zero flip the color of the border to
        // indicate an error.
        if ( phaseDataStatusObj.val === 0 )
        {
            this.layout.xaxis.linecolor = 'limegreen';
            this.layout.yaxis.linecolor = 'limegreen';
        }
        else
        {
            this.layout.xaxis.linecolor = 'red';
            this.layout.yaxis.linecolor = 'red';
        }

        // Create a trace array which will display the current value.
        let plotTraceObjects = this.makePlotlyTraceChannelValueOK( phaseDataValueObj.val, tooltipText );
        this.react( plotTraceObjects );
    }

    react( plotTraceObjects )
    {
        Plotly.react( this.plotElement, plotTraceObjects, this.layout, { displayModeBar: false} );
    }

    makePlotlyTraceChannelValueOK( barCurrentValue, tooltipText )
    {
        const needleWidth = ( NEEDLE_WIDTH_IN_PERCENT / 100 ) * ( this.rangeMax - this.rangeMin );
        const guardWidth = ( GUARD_WIDTH_IN_PERCENT / 100 ) * ( this.rangeMax - this.rangeMin );

        const rangeLimitedValue = this.rangeLimit( barCurrentValue, this.rangeMin + guardWidth, this.rangeMax - needleWidth - guardWidth );

        const trace1 = {
            x: [ rangeLimitedValue ],
            y: ['x'],
            type: 'bar',
            text: "",
            textposition: 'inside',
            // make the first colour in the stack the same as the background
            // to create a linear needle effect
            marker: {color: 'rgba(0,0,0,0)'},
            orientation: 'h',
            hoverinfo: 'none',
        };

        // When the current value is out-of-range flip the colour to indicate
        const needleColor = ( ( barCurrentValue < this.rangeMin ) || ( barCurrentValue > this.rangeMax ) ) ? "red" : "limegreen";

        const trace2 = {
            x: [ needleWidth ],
            y: ['x'],
            type: 'bar',
            text: "",
            textposition: 'outside',
            marker: {color: needleColor },
            orientation: 'h',
            name: tooltipText,
            hoverinfo: 'name',
        };

        // Position the text label in the first or second trace according to whether
        // the needle is at greater than 50% full scale deflection.
        if ( ( rangeLimitedValue / this.rangeMax ) > 0.5 )
        {
            trace1.text = barCurrentValue.toFixed(1) + "°";
        }
        else
        {
            trace2.text = barCurrentValue.toFixed(1) + "°";
        }

        return [ trace1, trace2 ];
    }

    makePlotlyTraceWicaStreamDisconnected( tooltipText )
    {
        return this.makePlotlyTraceChannelException( tooltipText, "Wica Stream Not Connected", "white" );
    }

    makePlotlyTraceEpicsChannelDisconnected( tooltipText )
    {
        return this.makePlotlyTraceChannelException( tooltipText, "EPICS Channel Not Connected", "white" );
    }

    makePlotlyTraceChannelException( tooltipText, errorMessage, errorColor )
    {
        let trace1 = {
            x: [ this.rangeMin ],
            y: ['x'],
            type: 'bar',
            orientation: 'h',
            hoverinfo: 'none'
        };

        let trace2 = {
            x: [ this.rangeMax - this.rangeMin ],
            y: ['x'],
            type: 'bar',
            orientation: 'h',
            marker: {color: errorColor },
            text: errorMessage,
            name: tooltipText,
            textposition: 'inside',
            hoverinfo: 'name'
        };

        return [ trace1, trace2 ];
    }

    rangeLimit( n, min, max ) {

        if ( n < min ) {
            return min;
        }
        else if ( n > max ) {
            return max;
        }
        else {
            return n;
        }
    }

    static buildFormattedTooltipText( element )
    {
        return element.getAttribute( "data-wica-channel-name" );
    }

    /**
     * Log any error data generated in this class.
     *
     * @private
     * @param {string} msg - custom error message.
     * @param {Error} err - the Error object
     */
    static logExceptionData_( msg, err )
    {
        let vDebug = "";
        for ( const prop in err )
        {
            if ( Object.hasOwnProperty.call( err, prop ) )
            {
                vDebug += "property: " + prop + " value: [" + err[ prop ] + "]\n";
            }
        }
        vDebug += "Details: [" + err.toString() + "]";
        log.warn( msg + vDebug );
    }

}
