import * as log from "./logger.js"

import Plotly from "plotly.js-dist-min";
import JSON5 from 'json5';

const BAR1_COLOR = "darkblue";
const BAR2_COLOR = "limegreen";
const BAR3_COLOR = "red";
const BAR4_COLOR = "black";

const BAR1_UNITS = "kVp";
const BAR2_UNITS = "kW "; // For the plot to work as expected it's important
const BAR3_UNITS = "kW";  // that the values for xxxUnits are unique. Hence, the space above (hack !)
const BAR4_UNTIS = "mbar";

const OTHER_CAVITY_VACUUM_MIN = 1E-7;
const OTHER_CAVITY_VACUUM_MAX = 1E-3;

const SB_CAVITY_VACUUM_MIN = 1E-7
const SB_CAVITY_VACUUM_MAX = 1E-5

class CavityStatusPlot
{
    constructor( plotTitle, plotDivId, ...barPropertyArgs )
    {
        this.plotTitle = plotTitle;
        this.plotElement = document.getElementById( plotDivId );
        this.barPropertyArgs= barPropertyArgs;

        this.layout = {
            title: this.plotTitle,
            titlefont: { size: 14, color: 'darkblue' },
            autosize: true,
            showlegend: false,
            margin: {l: 10, r: 10, b: 40, t: 40},
            paper_bgcolor: 'rgba(0,0,0,0)',
            plot_bgcolor: 'rgba(0,0,0,0)',
            yaxis: { range : [ 0, 101 ], dtick: 25, showticklabels: false },
            barmode: 'group',
            dragmode: false,
            hoverlabel: { namelength: -1, font: { size: 10 } }
        };
    }

    /**
     * Activates the plot. That's to stay starts rendering the associated plot object
     * based on information obtained from the configured wica html elements.
     *
     * @param {Number} [refreshRateInMilliseconds = 500] - The rate at which the plot should update.
     */
    activate( refreshRateInMilliseconds = 500 )
    {
        this.startPlotCycle( refreshRateInMilliseconds );
    }

    startPlotCycle( refreshRateInMilliseconds )
    {
        try
        {
            this.updatePlot();
        }
        catch( err )
        {
            CavityStatusPlot.logExceptionData_("Programming Error: updatePlot threw an exception: ", err );
        }

        // Reschedule next update
        setTimeout( () => this.startPlotCycle( refreshRateInMilliseconds ), refreshRateInMilliseconds );
    }

    updatePlot()
    {
        let plotTraceObjects = [];

        for ( let argIndex = 0; argIndex < this.barPropertyArgs.length; argIndex++ )
        {
            let barProps = this.barPropertyArgs[ argIndex ];
            let barElement = document.getElementById( barProps.elementId );
            let tooltipText = CavityStatusPlot.buildFormattedTooltipText( barElement );

            // If the WICA Stream is disconnected create a trace object that will show this condition.
            if  ( ( ! ( barElement.hasAttribute( "data-wica-stream-state" ) ) ) ||
                  ( ! ( barElement.hasAttribute( "data-wica-channel-metadata" ) ) ) ||
                  ( ! ( barElement.hasAttribute( "data-wica-channel-value-latest" ) ) ) ||
                  ( ! ( barElement.getAttribute( "data-wica-stream-state" ).startsWith( "opened-" ) ) ) )
            {
                plotTraceObjects[ argIndex ] = CavityStatusPlot.makePlotlyTraceWicaStreamDisconnected( barProps.name, tooltipText );
                continue;
            }

            // Obtain the channel value object
            let channelValueObj = JSON5.parse( barElement.getAttribute( "data-wica-channel-value-latest" ) );

            // If the EPICS Channel is disconnected create a trace object that will show this condition.
            if ( channelValueObj.val === null )
            {
                plotTraceObjects[ argIndex ] = CavityStatusPlot.makePlotlyTraceEpicsChannelDisconnected( barProps.name, tooltipText );
                continue;
            }

            // Obtain the required plot attributes from the channel value, the metadata objects and the supplied local properties
            const channelMetadataObj = JSON5.parse( barElement.getAttribute( "data-wica-channel-metadata" ) );
            const barMinValue = ( barProps.min === undefined) ? channelMetadataObj.lopr : barProps.min;
            const barMaxValue = ( barProps.max === undefined) ? channelMetadataObj.hopr : barProps.max;
            const barValueUseExponentFormat = ( barProps.useExpFormat === undefined) ? false : barProps.useExpFormat;
            const barValueFractionDigits = ( barProps.fractionDigits === undefined) ? 0: barProps.fractionDigits;
            const barGraphicUseLogarithmicScale = ( barProps.useLogScale === undefined) ? false : barProps.useLogScale;
            const barCurrentValue = channelValueObj.val;

            // Create a trace object which will display the current value
            plotTraceObjects[ argIndex ] = CavityStatusPlot.makePlotlyTraceChannelValueOK( barCurrentValue,
                barMinValue, barMaxValue,
                barProps.name, barProps.color,
                barValueUseExponentFormat,
                barValueFractionDigits,
                barGraphicUseLogarithmicScale,
                tooltipText );
        }

        Plotly.react( this.plotElement, plotTraceObjects, this.layout, { displayModeBar: false} );
    }

    static makePlotlyTraceChannelValueOK( barCurrentValue, barMinValue, barMaxValue, barName, barColor, barValueTextUseExponentFormat, barValueFractionDigits, barGraphicUseLogarithmicScale, tooltipText )
    {
        const barValue =  barGraphicUseLogarithmicScale ? CavityStatusPlot.scaleLogarithmically( barCurrentValue, barMinValue, barMaxValue ) :
            CavityStatusPlot.scaleLinearly( barCurrentValue, barMinValue, barMaxValue )

        const barValueText = barValueTextUseExponentFormat ? barCurrentValue.toExponential( 1 ) : barCurrentValue.toFixed( barValueFractionDigits );
        return {
            y: [ CavityStatusPlot.rangeLimit( barValue) ],
            x: [ barName ],
            text: barValueText,
            textposition: 'outside',
            type: 'bar',
            marker: {color: barColor },
            name: tooltipText,
            hoverinfo: 'name',
        };
    }

    static makePlotlyTraceWicaStreamDisconnected( barName, tooltipText )
    {
        return {
            y: [100],
            x: [ barName ],
            type: 'bar',
            text: "Wica Stream Not Connected",
            textposition: 'inside',
            marker: {color: "white"},
            name: tooltipText,
            hoverinfo: 'name'
        };
    }

    static makePlotlyTraceEpicsChannelDisconnected( barName, tooltipText )
    {
        return {
            y: [100],
            x: [ barName ],
            type: 'bar',
            text: "EPICS Channel Not Connected",
            textposition: 'inside',
            marker: {color: "white"},
            name: tooltipText,
            hoverinfo: 'name'
        };
    }

    static scaleLinearly( value, min, max )
    {
        return 100 * ( value - min ) / ( max - min );
    }

    static scaleLogarithmically( value, min, max )
    {
        const log10_min = Math.log10( min );
        const log10_max = Math.log10( max );
        return 100 * ( Math.log10( value ) - log10_min ) / ( log10_max - log10_min );
    }

    /**
     * Ensures the bar graph plot value does not exceed the allowed range (1-100), thus
     * ensuring that the bar is always slightly visible (when the value is below the
     * minimum) and that it does not overflow the bounds of the plot area.
     *
     * @param n the number to tange limit.
     * @return {number|*} the range limited values
     */
    static rangeLimit( n )
    {
        if ( n < 1 )
        {
            return 1;
        }
        else if ( n > 100 )
        {
            return 100;
        }
        else
        {
            return n;
        }
    }

    static buildFormattedTooltipText( element )
    {
        return element.getAttribute( "data-wica-channel-name" );
    }

    /**
     * Log any error data generated in this class.
     *
     * @private
     * @param {string} msg - custom error message.
     * @param {Error} err - the Error object
     */
    static logExceptionData_( msg, err )
    {
        let vDebug = "";
        for ( const prop in err )
        {
            if ( Object.hasOwnProperty.call( err, prop ) )
            {
                vDebug += "property: " + prop + " value: [" + err[ prop ] + "]\n";
            }
        }
        vDebug += "Details: [" + err.toString() + "]";
        log.warn( msg + vDebug );
    }
}

export class Inj2CWBCavityStatusMonitor extends CavityStatusPlot
{
    constructor( plotTitle, plotDivId, spannungDataId, hinlaufDataId, reflexionDataId, vacuumDataId )
    {
        const bar1Props = { elementId: spannungDataId,  name: "kVp", color: BAR1_COLOR, min: 0, max:   29 };
        const bar2Props = { elementId: hinlaufDataId,   name: "W", color: BAR2_COLOR, min: 0, max:  400 };
        const bar3Props = { elementId: reflexionDataId, name: " W ", color: BAR3_COLOR, min: 0, max:  200, fractionDigits: 1 };
        const bar4Props = { elementId: vacuumDataId,    name: "mbar", color: BAR4_COLOR, min: OTHER_CAVITY_VACUUM_MIN, max: OTHER_CAVITY_VACUUM_MAX, useExpFormat: true, useLogScale: true };

        super( plotTitle, plotDivId, bar1Props, bar2Props, bar3Props, bar4Props);
    }
}

export class Inj2CWB3CavityStatusMonitor extends CavityStatusPlot
{
    constructor( plotTitle, plotDivId, spannungDataId, hinlaufDataId, reflexionDataId )
    {
        const bar1Props = { elementId: spannungDataId,  name: "V", color: BAR1_COLOR, min: 0, max: 10, fractionDigits: 1 };
        const bar2Props = { elementId: hinlaufDataId,   name: " V ", color: BAR2_COLOR, min: 0, max:  10, fractionDigits: 1 };
        const bar3Props = { elementId: reflexionDataId, name: "  V  ", color: BAR3_COLOR, min: 0, max:   10, fractionDigits: 1 };

        super( plotTitle, plotDivId, bar1Props, bar2Props, bar3Props );
    }
}

export class Inj2FourChannelCavityStatusMonitor extends CavityStatusPlot
{
    constructor( plotTitle, plotDivId, spannungDataId, hinlaufDataId, reflexionDataId, vacuumDataId )
    {
        const bar1Props = { elementId: spannungDataId,  name: BAR1_UNITS, color: BAR1_COLOR, min: 0, max: 500 };
        const bar2Props = { elementId: hinlaufDataId,   name: BAR2_UNITS, color: BAR2_COLOR, min: 0, max: 300 };
        const bar3Props = { elementId: reflexionDataId, name: BAR3_UNITS, color: BAR3_COLOR, min: 0, max:  30, fractionDigits: 1 };
        const bar4Props = { elementId: vacuumDataId,    name: BAR4_UNTIS, color: BAR4_COLOR, min: OTHER_CAVITY_VACUUM_MIN, max: OTHER_CAVITY_VACUUM_MAX, useExpFormat: true, useLogScale: true };

        super( plotTitle, plotDivId, bar1Props, bar2Props, bar3Props, bar4Props);
    }
}

export class Inj2CI4CavityStatusMonitor extends CavityStatusPlot
{
    constructor( plotTitle, plotDivId, spannungDataId, hinlaufDataId, reflexionDataId, vacuumDataId )
    {
        const bar1Props = { elementId: spannungDataId,  name: BAR1_UNITS, color: BAR1_COLOR, min: 0, max: 50 };
        const bar2Props = { elementId: hinlaufDataId,   name: BAR2_UNITS, color: BAR2_COLOR, min: 0, max: 30 };
        const bar3Props = { elementId: reflexionDataId, name: BAR3_UNITS, color: BAR3_COLOR, min: 0, max:  3, fractionDigits: 1 };
        const bar4Props = { elementId: vacuumDataId,    name: BAR4_UNTIS, color: BAR4_COLOR, min: OTHER_CAVITY_VACUUM_MIN, max: OTHER_CAVITY_VACUUM_MAX, useExpFormat: true, useLogScale: true };

        super( plotTitle, plotDivId, bar1Props, bar2Props, bar3Props, bar4Props);
    }
}

export class RingFourChannelCavityStatusMonitor extends CavityStatusPlot
{
    constructor( plotTitle, plotDivId, spannungDataId, hinlaufDataId, reflexionDataId, vacuumDataId )
    {
        const bar1Props = { elementId: spannungDataId,  name: BAR1_UNITS, color: BAR1_COLOR, min: 0, max: 1000 };
        const bar2Props = { elementId: hinlaufDataId,   name: BAR2_UNITS, color: BAR2_COLOR, min: 0, max:  800 };
        const bar3Props = { elementId: reflexionDataId, name: BAR3_UNITS, color: BAR3_COLOR, min: 0, max:   40, fractionDigits: 1 };
        const bar4Props = { elementId: vacuumDataId,    name: BAR4_UNTIS, color: BAR4_COLOR, min: OTHER_CAVITY_VACUUM_MIN, max: OTHER_CAVITY_VACUUM_MAX, useExpFormat: true, useLogScale: true };

        super( plotTitle, plotDivId, bar1Props, bar2Props, bar3Props, bar4Props);
    }
}

export class RingFlatTopCavityStatusMonitor extends CavityStatusPlot
{
    constructor( plotTitle, plotDivId, spannungDataId, hinlaufDataId, reflexionDataId, vacuumDataId )
    {
        const bar1Props = { elementId: spannungDataId,  name: BAR1_UNITS, color: BAR1_COLOR, min: 0, max: 1000 };
        const bar2Props = { elementId: hinlaufDataId,   name: BAR2_UNITS, color: BAR2_COLOR, min: 0, max:  200, fractionDigits: 1 };
        const bar3Props = { elementId: reflexionDataId, name: BAR3_UNITS, color: BAR3_COLOR, min: 0, max:  200, fractionDigits: 1 };
        const bar4Props = { elementId: vacuumDataId,    name: BAR4_UNTIS, color: BAR4_COLOR, min: OTHER_CAVITY_VACUUM_MIN, max: OTHER_CAVITY_VACUUM_MAX, useExpFormat: true, useLogScale: true };

        super( plotTitle, plotDivId, bar1Props, bar2Props, bar3Props, bar4Props);
    }
}

export class RingSBCavityStatusMonitor extends CavityStatusPlot
{
    constructor( plotTitle, plotDivId, spannungDataId, hinlaufDataId, reflexionDataId, vacuumDataId )
    {
        const bar1Props = { elementId: spannungDataId,  name: BAR1_UNITS, color: BAR1_COLOR };
        const bar2Props = { elementId: hinlaufDataId,   name: BAR2_UNITS, color: BAR2_COLOR };
        const bar3Props = { elementId: reflexionDataId, name: BAR3_UNITS, color: BAR3_COLOR, fractionDigits: 1 };
        const bar4Props = { elementId: vacuumDataId,    name: BAR4_UNTIS, color: BAR4_COLOR, min: SB_CAVITY_VACUUM_MIN, max: SB_CAVITY_VACUUM_MAX, useExpFormat: true, useLogScale: true };

        super( plotTitle, plotDivId, bar1Props, bar2Props, bar3Props, bar4Props);
    }
}
