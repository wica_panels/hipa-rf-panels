import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import copy from "rollup-plugin-copy-assets";

export default [ {
        input:  'src/rf-plot-support.js',
        output: {
            dir: 'build',
            format: 'esm',
            sourcemap: true, // create map to facilitate in-browser debugging
        },
        plugins: [
            resolve(),  // so Rollup can find modules.
            commonjs(), // so Rollup can convert the input to an ES module.
            copy( {
                assets: [
                    "src/rf-svg-support.js",
                    "src/rf-status-support.js",
                    "src/inj2.html",
                    "src/inj2-res2.html",
                    "src/inj2-res4.html",
                    "src/rf-station-styles.css",
                    "src/ring.html",
                    "src/rf_hipa.svg",
                    "src/rf_defs.svg"
                ],
            })
        ]
    }
];